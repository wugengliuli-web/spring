/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : users

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-04-29 10:13:50
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `hotal`
-- ----------------------------
DROP TABLE IF EXISTS `hotal`;
CREATE TABLE `hotal` (
  `hotalmanager` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hotalguest` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotalname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hotalmoney` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hotal
-- ----------------------------
INSERT INTO `hotal` VALUES ('123456', '1', '444', '娱乐天地', '100');
INSERT INTO `hotal` VALUES ('123456', '6854', null, '123', '123');
