package com.example.mybatisplus.table;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("hotal")
@Data
public class Hotal {
    @TableId(value = "id")
    private String id;

    @TableField(value = "hotalguest")
    private String hotalguest;

    @TableField(value = "hotalname")
    private String hotalname;

    @TableField(value = "hotalmoney")
    private int hotalmoney;

    @TableField(value = "hotalmanager")
    private String hotalmanager;
}
