package com.example.mybatisplus.table;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("users")
@Data
public class User {
    @TableId(value="account")
    private String account;

    @TableField("password")
    private String password;

    @TableField("username")
    private String username;

    @TableField("usertype")
    private int usertype;
}
