package com.example.mybatisplus.utils;

import java.util.UUID;

public class CreateId {
    public static String uniqueId() {
        String id = UUID.randomUUID().toString().substring(0,4);
        System.out.println(id);
        return id;
    }
}
