package com.example.mybatisplus.control;

import com.example.mybatisplus.dao.UserMapper;
import com.example.mybatisplus.table.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {Cors.corsURl, "null"})
@RestController
public class UserControl {
    @Autowired
    private UserMapper userMapper;

    @PostMapping(value = "/add-user")
    public Map<String, Object> addUser(@RequestBody User user) {
        User newUser = new User();
        String account = user.getAccount();
        User hasUser = userMapper.selectById(account);
        MyResponse response;
        if(hasUser != null) {
            response = new MyResponse(Code.Error, "创建失败，账户重复");
        } else {
            newUser.setUsername(user.getUsername());
            newUser.setAccount(user.getAccount());
            newUser.setPassword(user.getPassword());
            newUser.setUsertype(user.getUsertype());
            userMapper.insert(newUser);
            response = new MyResponse(Code.Success, "创建成功");
        }
        return response.sendJson();
    }

    @GetMapping(value = "/users")
    public Map<String, Object> users() {
        List<User> list = userMapper.selectList(null);
        MyResponse response = new MyResponse(Code.Success, "查询成功", list);
        return response.sendJson();
    }

    @GetMapping(value = "/user")
    public Map<String, Object> user(@RequestParam("account") String account) {
        User user = userMapper.selectById(account);
        MyResponse response;
        if(user == null) {
            response = new MyResponse(Code.Error, "账户不存在");
        } else {
            response = new MyResponse(Code.Success, "查找成功", user);
        }
        return response.sendJson();
    }

    @PostMapping(value = "/change-user")
    public Map<String, Object> changeUser(@RequestBody User user) {
        int res = userMapper.updateById(user);
        MyResponse response;
        if(res > 0) {
            response = new MyResponse(Code.Success, "修改成功");
        } else {
            response = new MyResponse(Code.Error, "修改失败");
        }
        return response.sendJson();
    }
}
