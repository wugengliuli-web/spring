package com.example.mybatisplus.control;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplus.dao.HotalMapper;
import com.example.mybatisplus.interfaceclass.Arrange;
import com.example.mybatisplus.table.Hotal;
import com.example.mybatisplus.utils.CreateId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {Cors.corsURl, "null"})
@RestController
public class HotalControl {
    @Autowired
    HotalMapper hotalMapper;

    @GetMapping("/hotals")
    public Map<String, Object> hotals() {
        List<Hotal> list = hotalMapper.selectList(null);
        MyResponse response = new MyResponse(Code.Success, "查询成功", list);
        return response.sendJson();
    }

    @PostMapping("/arrange")
    public Map<String, Object> arrange(@RequestBody Arrange arrange) {
        Hotal hotal = hotalMapper.selectById(arrange.id);
        MyResponse response;
        if(hotal.getHotalguest() != null) {
            response = new MyResponse(Code.Error, "已被预订");
        } else {
            hotal.setHotalguest(arrange.account);
            int res = hotalMapper.updateById(hotal);
            if(res >0) {
                response = new MyResponse(Code.Success, "成功预订");
            } else {
                response = new MyResponse(Code.Error, "预订失败");
            }

        }
        return response.sendJson();
    }

    @PostMapping("/add-hotal")
    public Map<String, Object> addHotal(@RequestBody Hotal hotal) {
        String hotalname = hotal.getHotalname();
        QueryWrapper<Hotal> wrapper = new QueryWrapper<>();
        wrapper.eq("hotalname", hotalname);
        Hotal hotalOrNull = hotalMapper.selectOne(wrapper);
        System.out.println(hotalOrNull);
        MyResponse response;
        if(hotalOrNull == null) {
            String id = CreateId.uniqueId();
            hotal.setId(id);
            int res = hotalMapper.insert(hotal);

            if(res>0) {
                response = new MyResponse(Code.Success, "创建成功");
            } else {
                response = new MyResponse(Code.Error, "创建失败");
            }
        } else {
            response = new MyResponse(Code.Error, "出现重复旅店名");
        }

        return response.sendJson();
    }
}
