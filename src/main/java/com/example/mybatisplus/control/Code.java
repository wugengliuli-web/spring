package com.example.mybatisplus.control;

public interface Code {
    int Success = 1;
    int Error = 2;
    int Timeout = 3;
};
