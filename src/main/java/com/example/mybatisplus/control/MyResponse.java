package com.example.mybatisplus.control;

import java.util.HashMap;
import java.util.Map;

public class MyResponse {
    int code;
    String msg;
    Object value;
    MyResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    MyResponse(int code, String msg, Object value) {
        this.code = code;
        this.msg = msg;
        this.value = value;
    }

    public Map<String, Object> sendJson() {
        Map<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("msg", msg);
        map.put("value", value);
        return map;
    }
}
