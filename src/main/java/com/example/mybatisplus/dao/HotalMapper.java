package com.example.mybatisplus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mybatisplus.table.Hotal;

public interface HotalMapper extends BaseMapper<Hotal> { }
