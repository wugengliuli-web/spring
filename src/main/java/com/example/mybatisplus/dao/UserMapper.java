package com.example.mybatisplus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mybatisplus.table.User;

public interface UserMapper extends BaseMapper<User> { }
